package com.example.tubesboyor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTitle("Login Menu")

        var btn = findViewById<Button>(R.id.LgnBtn)
        var username = findViewById<EditText>(R.id.UTxt);
        var pass = findViewById<EditText>(R.id.PTxt);

        btn.setOnClickListener(View.OnClickListener(){
            var user = username.text.toString()
            var password = pass.text.toString()

            if (user == "admin" && password == "admin"){
                Toast.makeText(this, "Login Sukses", Toast.LENGTH_LONG).show()
                var i = Intent(this,Menu::class.java);
                startActivity(i);
            }
            else {
                Toast.makeText(this, "Username atau Password Salah", Toast.LENGTH_LONG).show()
            }
        })
    }
}