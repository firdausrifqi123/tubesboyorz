package com.example.tubesboyor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        setTitle("Menu")
        var btn1 = findViewById<Button>(R.id.FBbtn)
        var btn2 = findViewById<Button>(R.id.FNbtn)
        var btn3 = findViewById<Button>(R.id.FRbtn)

        btn1.setOnClickListener(View.OnClickListener {
            var i = Intent(this,FitnessBerat::class.java);
            startActivity(i)
        })
        btn2.setOnClickListener(View.OnClickListener {
            var i = Intent(this,FitnessNormal::class.java);
            startActivity(i)
        })
        btn3.setOnClickListener(View.OnClickListener {
            var i = Intent(this,FitnessRingan::class.java);
            startActivity(i)
        })
    }
}